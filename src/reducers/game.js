import { arrayMove } from 'react-sortable-hoc';

export const gameTypes = ["Yahtzee" , "Poker"] ;

const gameReducerDefaultState = {
    type : gameTypes[0],
    creationData: {
        stepTitles : ["Game type" , "Players", "Confirmation" ],
        step : 0,
        selectedPlayer : undefined
    },
    players : []
};

export default (state = gameReducerDefaultState, action) => {
    switch(action.type){
        case 'ADD_PLAYER': 
            var players = state.players.concat([action.player]);
            return { ...state, players } ;
        case 'REMOVE_PLAYER': 
            var players = state.players.filter((item) => action.player !== item);
            return { ...state, players } ;
        case 'REMOVE_PLAYERS': 
            var players = [];
            return { ...state, players } ;
        case 'INCREMENT_GAME_CREATION_STEP':
            return {
                ...state,
                creationData : {
                        ... state.creationData,
                        step: state.creationData.step + 1
                    }
                };
        case 'DECREMENT_GAME_CREATION_STEP':
            return { 
                ...state,
                creationData : {
                        ... state.creationData,
                        step: state.creationData.step - 1
                    }
                };
        case 'UPDATE_GAME_TYPE':
            return { 
                ...state,
                type : action.gameType
                };
        case 'UPDATE_PLAYER_ORDER':
            var statePlayers = [...state.players];
            var players = arrayMove(statePlayers, action.oldIndex, action.newIndex);
            return { ...state, players } ;
        default:
            return state; 
    }
}; 