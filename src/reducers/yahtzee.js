import gameReducerDefaultState from './game';

const yahtzeeReducerDefaultState = {
    playerTurnIndex : 0,
    scores: [],
    strikes: [ 
        { name: "ones", increment: true, points: 1, occurrence: 0, scores: [] }, 
        { name:  "twos", increment: true, points: 2, occurrence: 0, scores: [] }, 
        { name: "threes", increment: true, points: 3, occurrence: 0, scores: [] }, 
        { name: "fours", increment: true, points: 4, occurrence: 0, scores: [] }, 
        { name: "fives", increment: true, points: 5, occurrence: 0, scores: [] }, 
        { name: "sixes", increment: true, points: 6, occurrence: 0, scores: [] }, 
        { name: "total top", automatic: true }, 
        { name: "bonus", automatic: true }, 
        { name: "three of a kind", increment: true, points: 3, occurrence: 0, scores: [] }, 
        { name: "four of a kind", increment: true, points: 4, occurrence: 0, scores: [] }, 
        { name: "full house", increment: false, points: 25, unlocked: false, scores: [] }, 
        { name: "small straight", increment: false, points: 30, unlocked: false, scores: [] }, 
        { name: "large straight", increment: false, points: 40, unlocked: false, scores: [] }, 
        { name: "chance", increment: true, points: 1, unlocked: false, scores: [] }, 
        { name: "yahtzee", increment: false, points: 50, unlocked: false, scores: [] },
        { name: "total bottom", automatic: true }, 
        { name: "Big total", automatic: true }
    ]
};

export default (state = yahtzeeReducerDefaultState, action) => {
    switch(action.type){
        case 'PLAY_STRIKE':
            let strike = action.strike;
            if (!strike.scores[action.playerId])
                strike.scores[action.playerId] = 0; 
    
            const points = strike.increment ?
                strike.scores[action.playerId] / strike.points < 6 ? strike.scores[action.playerId] + strike.points : strike.scores[action.playerId] :
                strike.points;
            
            strike.scores[action.playerId] = points;
            
            const strikes = state.strikes.map((cstrike)=>{
                return cstrike.name === strike.name ? strike : cstrike;
            });
            return { ...state, strikes };
        case 'UPDATE_PLAYER_TURN':
            return state.playerTurnIndex + 1;
        default:
            return state; 
    }
}; 