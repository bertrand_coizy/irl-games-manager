import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import gameReducerDefaultState from '../reducers/game';
import yahtzeeReducerDefaultState from '../reducers/yahtzee'
import thunk from 'redux-thunk';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION__COMPOSE__ || compose;

export default () => {
    const store = createStore(
        combineReducers({
            game: gameReducerDefaultState,
            yahtzee : yahtzeeReducerDefaultState
        }),
        composeEnhancers(applyMiddleware(thunk))
    );
    
    console.log(store.getState())
    return store;
}
