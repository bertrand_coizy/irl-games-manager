import * as firebase from 'firebase';
import * as gameActions from '../actions/game.js';
import * as yahtzeeActions from '../actions/yahtzee.js';

const config = {
    apiKey: process.env.FIREBASE_API_KEY,
    authDomain: process.env.FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.FIREBASE_DATABASE_URL,
    projectId: process.env.FIREBASE_PROJECT_ID,
    storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID
  };
  

firebase.initializeApp(config);

const database = firebase.database();
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();

database.ref('game').on('child_removed', (snapshot) => {
    console.log(snapshot.key, snapshot.val());
});

database.ref('game').on('child_changed', (snapshot) => {
    console.log(snapshot.key, snapshot.val());
});

database.ref('game').on('child_added', (snapshot) => {
    console.log(snapshot.key, snapshot.val());
});
export { firebase, googleAuthProvider, database as default };

// database.ref().set({
//      name: 'Bertrand Coizy',
//      age: 38,
//      isSingle: false,
//      location: {
//          city: 'Montréal',
//         country:'Canada'
//      }
//  }).then(() => {
//      console.log('Data is saved');
//  }).catch((error) => {
//      console.log('This failed', error);
// });

// database.ref().update({
//     name: 'trambz',
//     age: 29
// }).then(()=>{ 
//     console.log("data have been updated!");
// }).catch((error)=>{console.log("delete failed")})