import React from 'react';
import { connect } from 'react-redux';

class Poker extends React.Component{
    constructor(props){
        super(props);
    }
   
    render = () => {
        return (
            <div className="container">
                <h2>Sorry, this type of game is still under development</h2>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return { 
        poker: state.poker,
        players: state.game.players
    }
}

export default connect(mapStateToProps)(Poker);