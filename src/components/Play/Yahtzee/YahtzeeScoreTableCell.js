import React from 'react';
import { connect } from 'react-redux';

import { playStrike } from '../../../actions/yahtzee';

class YahtzeeScoreTableCell extends React.Component{
    constructor(props){
        super(props);
        this.playerId = this.props.players.indexOf(props.player);
    }
    onPlayStrike = () => {
        this.props.dispatch(playStrike(this.playerId, this.props.strike)); 
    }
    render = () => {
        return (<td onClick={this.onPlayStrike} key={this.props.player} >{this.props.score}</td>);
    }
}

const mapStateToProps = (state) => {
    return { 
        game: state.game,
        players: state.game.players,
        strikes: state.game.strikes
    }
}

export default connect(mapStateToProps)(YahtzeeScoreTableCell);