import React from 'react';
import { connect } from 'react-redux';

import YahtzeeScoreTableCell from './YahtzeeScoreTableCell';

class YahtzeeScoreTableBody extends React.Component{
    constructor(props){
        super(props);
        this.playerId = this.props.players.indexOf(this.props.player);
    }
   
    render = () => {
        return (
            <tbody>
                { 
                    this.props.yahtzee.strikes.map((strike)=> {
                        return <tr key={strike.name}><td>{strike.name}</td>{
                            this.props.players.map((player,i) => { 
                                return <YahtzeeScoreTableCell score={strike.scores && strike.scores[i] ? strike.scores[i] : 0} key={`${player}${strike.name}`} player={player} strike={strike} />
                            })}
                            </tr>
                })}
            </tbody>
        );
    }
}
const mapStateToProps = (state) => {
    return { 
        yahtzee: state.yahtzee,
        players: state.game.players
    }
}

export default connect(mapStateToProps)(YahtzeeScoreTableBody);