import React from 'react';
import { connect } from 'react-redux';

import YahtzeeScoreTable from './YahtzeeScoreTable';

class Yahtzee extends React.Component{
    constructor(props){
        super(props);
    }
   
    render = () => {
        return (
            <div className="container">
                <h2>Yathzee</h2>
                <div>
                    <YahtzeeScoreTable />
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return { 
        yahtzee: state.yahtzee,
        players: state.game.players
    }
}

export default connect(mapStateToProps)(Yahtzee);