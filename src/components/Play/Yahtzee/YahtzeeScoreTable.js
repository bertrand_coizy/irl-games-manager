import React from 'react';
import { connect } from 'react-redux';

import YahtzeeScoreTableHeader from './YahtzeeScoreTableHeader';
import YahtzeeScoreTableBody from './YahtzeeScoreTableBody';

class YahtzeeScoreTable extends React.Component{
    constructor(props){
        super(props);
    }
   
    render = () => {
        return (
            <table>
                <YahtzeeScoreTableHeader />
                <YahtzeeScoreTableBody />
            </table>
        );
    }
}
const mapStateToProps = (state) => {
    return { 
        yahtzee: state.yahtzee,
        players: state.game.players
    }
}

export default connect(mapStateToProps)(YahtzeeScoreTable);