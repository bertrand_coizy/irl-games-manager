import React from 'react';
import { connect } from 'react-redux';

class YahtzeeScoreTableCell extends React.Component{
    constructor(props){
        super(props);
    }
   
    render = () => {
        return (
            <thead>
                <tr><th className='topLeftEmptyCell'></th>
                {
                    this.props.players.map((player) => { 
                        return <th key={player}>{player}</th>
                    })
                }   
                </tr>
            </thead>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        players: state.game.players
    }
}

export default connect(mapStateToProps)(YahtzeeScoreTableCell);
                    
            