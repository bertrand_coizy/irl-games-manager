import React from 'react';
import { connect } from 'react-redux';

import { gameTypes } from '../../reducers/game';
import Poker from './Poker/Poker';
import Yahtzee from './Yahtzee/Yahtzee';

class PlayPage extends React.Component{
    constructor(props){
        super(props);
    }
   
    render = () => {
        return (
            <div className="container">
                { this.props.game.type === "Yahtzee" && <Yahtzee />}
                { this.props.game.type === "Poker" && <Poker />}
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return { 
        game: {...state.game}
    }
}

export default connect(mapStateToProps)(PlayPage);