import React from 'react';

export default class AddOption extends React.Component{
    state = { error : undefined }
    
    clearDefaultCaption = (e) => {
        if (e.target.value === this.props.inputDefaultCaption){
            e.target.value = "";
        }
    }
    handleAddOption = (e) => {
        e.preventDefault();
        const option = e.target.elements.option.value.trim();
        const error = this.props.handleAddOption(option);
        this.setState(()=>({ error }));
        if (!error && option !== this.props.inputDefaultCaption){
            e.target.elements.option.value = "";
        }
    }

    render(){
        return (
            <div>
            {this.state.error && <p className="addOptionError">{this.state.error}</p>}
                <form className="addOption" onSubmit={this.handleAddOption}>
                    <input className="addOption__input" autoComplete="off" type="text" name="option" defaultValue={this.props.inputDefaultCaption}  onClick={this.clearDefaultCaption}/>
                    <button className="button">Add {this.props.optionType}</button>
                </form>
            </div>
        );
    }
}