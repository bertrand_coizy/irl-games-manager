import React from 'react';
import { connect } from 'react-redux';

import { gameTypes } from '../../reducers/game';
import {updateGameType} from '../../actions/game';

class GameSetup extends React.Component{
    constructor(props){
        super(props);
    }

    onGameTypeChange = (e) => {
        const gameType = e.target.value;
        this.props.dispatch(updateGameType(gameType));
    }
        render = (props) => (            
        <div>
            <div className='container'>
                <div className='container container-flex-line'>
                        <p>Please choose your game type.</p>
                                   
                        <select className='select-style' onChange={this.onGameTypeChange}>
                        { gameTypes.map((gameType,idx) => <option key={idx} >{gameType}</option>)}
                        </select>
                </div>
            </div>
        </div>);
                    
}
const mapStateToProps = (state) => {
    return {
        game: state.game
    }
}
export default connect(mapStateToProps)(GameSetup);