import React from 'react';
import { connect } from 'react-redux';

import { stepTitles } from '../../reducers/game';

class GameStepTitle extends React.Component{
    constructor(props){
        super(props);
    }

    render = () => (            
        <div className="widget-header">
            <h3 className="widget-header__title">{this.props.game.creationData.stepTitles[this.props.game.creationData.step]}</h3>
        </div>);
}       
const mapStateToProps = (state) => {
    return { 
        game: state.game
    }
}

export default connect(mapStateToProps)(GameStepTitle);
