import React from 'react';
import { connect } from 'react-redux';

import Options from '../Options';
import AddOption from '../AddOption';
import { addPlayer, removePlayer, removePlayers } from '../../actions/game';
import { SortableList, SortableItem } from '../Common/SortableList';
import { updatePlayerOrder } from '../../actions/game';
    

class PlayersSetup extends React.Component{
    constructor(props){
        super(props);
    }

    handleAddPlayer = (player) => {
        if (!player){
            return "Enter a valid value to add item.";
        }
        else if (this.props.game.players.indexOf(player) > -1){
            return "This player have already been added.";
        }
        this.props.dispatch(addPlayer(player));
    }

    handleDeletePlayer = (optionToRemove) => { this.props.dispatch(removePlayer(optionToRemove)); }

    handleDeletePlayers = () => { this.props.dispatch(removePlayers()); }
    
    onSortEnd = ({oldIndex, newIndex}) => {
        this.props.dispatch(updatePlayerOrder(oldIndex, newIndex));
    };
    render = (props) => {
        return (
        <div>
            <div className="container">Drag and drop to change order</div>
            <div className="container">
                <SortableList items={this.props.game.players} onSortEnd={this.onSortEnd}  />
            </div>         
            
            <AddOption handleAddOption={this.handleAddPlayer} options={this.props.game.players} optionType="player" inputDefaultCaption="Please enter player name..."/>
        </div>)};

}
const mapStateToProps = (state) => { 
    return {
        game: state.game
    }
}
export default connect(mapStateToProps)(PlayersSetup);