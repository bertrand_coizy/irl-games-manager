import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { incrementGameCreationStep, decrementGameCreationStep} from '../../actions/game';

class GameCreationNavigation extends React.Component{
    constructor(props){
        super(props);
        
    }
    incrementStep = () => { this.props.dispatch(incrementGameCreationStep()); }
    decrementStep = () => { this.props.dispatch(decrementGameCreationStep()); }

    render = () => {
        return (
            <div className="container-flex">
                {this.props.game.creationData.step <  this.props.game.creationData.stepTitles.length - 1 ? 
                    <button className="button" onClick={this.incrementStep} disabled={this.props.game.creationData.step === 1 && this.props.game.players.length < 2}>
                        Next step
                    </button> :
                    <Link className="button" to="/play">
                        Start game!
                    </Link> 
                }
                <button className="button" onClick={this.decrementStep} disabled={this.props.game.creationData.step === 0}>Back</button>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return { 
        game: state.game
    }
}

export default connect(mapStateToProps)(GameCreationNavigation);