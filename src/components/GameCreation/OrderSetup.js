import React from 'react';
import { connect } from 'react-redux';

import { SortableList, SortableItem } from '../Common/SortableList';
import { updatePlayerOrder } from '../../actions/game';

class OrderSetup extends React.Component{
    constructor(props){
        super(props);
    }

    render = () => {
        return (
        <div className="container">
            <SortableList items={this.props.game.players} onSortEnd={this.onSortEnd} />
        </div>);
        }
    
}
const mapStateToProps = (state) => {
    return { 
        game: state.game
    }
}

export default connect(mapStateToProps)(OrderSetup);

