import React from 'react';
import { connect } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import GameCreationNavigation from './GameCreationNavigation'
import GameInfo from './GameInfo'; 
import GameSetup from './GameSetup'; 
import PlayersSetup from './PlayersSetup';
import OrderSetup from './OrderSetup';
import GameStepTitle from './GameStepTitle';
import { stepTitles } from '../../reducers/game'

class CreateGamePage extends React.Component{
    constructor(props){
        super(props);
    }

    render = () => {
        return (
        <div className="container">
            <div className="container-flex-line">
                <h2>Game creation</h2>
                <h2>Step { this.props.game.creationData.step + 1 } / { this.props.game.creationData.stepTitles.length }</h2>
            </div>
            <div className="widget">
                <GameStepTitle />            
                { this.props.game.creationData.step > 0 && <GameInfo /> }
                { this.props.game.creationData.step === 0 && <GameSetup /> }
                { this.props.game.creationData.step === 1 && <PlayersSetup /> }
                <GameCreationNavigation />
            </div>
            
        </div>);
        }
    
}
const mapStateToProps = (state) => {
    return { 
        game: state.game
    }
}

export default connect(mapStateToProps)(CreateGamePage);
