import React from 'react';
import { connect } from 'react-redux';


class GameInfo extends React.Component{
    constructor(props){
        super(props);
    }

    render = () => (
        <div className='container-flex gameInfo' >
            { this.props.game.creationData.step > 0 && <p>Game type: {this.props.game.type}</p>}
            { this.props.game.creationData.step > 1 && <p>Players: {this.props.game.players.map((player,index) => {
                return `${player}${index !== this.props.game.players.length -1  ? ', ' :''}`
            })}</p>}
        </div>);

}
const mapStateToProps = (state) => {
    return {
        game: state.game
    }
}
export default connect(mapStateToProps)(GameInfo);