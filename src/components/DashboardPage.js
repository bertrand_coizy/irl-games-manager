import React from "react";
import { Link } from 'react-router-dom'

const DashboardPage = (props) => (
    <div className="container">
        <div className="container">
            <h2>Lorem ipsum...</h2>
            <div className="container-flex"> 
                <Link className="big-button" to="/create">Create a game</Link>
                <Link className="big-button" to="/statistics">Statistics</Link>
                </div>
        </div>
    </div>);

export default DashboardPage;