import React from 'react';
import {SortableContainer, SortableElement } from 'react-sortable-hoc';

export const SortableItem = SortableElement(({value}) =>
    <li>{value}</li>
);

export const SortableList = SortableContainer(({items}) => {
    return (
    <ul className='sortableList'>
        {items.map((value, index) => (
        <SortableItem key={`item-${index}`} index={index} value={value} />
        ))}
    </ul>
    );
});