import React from 'react'
import { connect } from 'react-redux';
import Option from './Option.js'

import configureStore from '../store/configureStore';

class Options extends React.Component{
      constructor(props){
        super(props);
    }
    render = () =>{ 
        return (
        <div>
            <div className="widget-header">
                <h3 className="widget-header__title">{this.props.widgetName}</h3>
                <div>
                    <p><button className="button button--link" onClick={this.props.handleDeleteOptions}>Remove All</button></p>
                </div>
            </div>
            { this.props.options.length === 0 && <p className="widgetElement">Please add an {this.props.optionType} to get started</p>}
            {
                this.props.options.map((option, index) => 
                    <Option          
                        key={option}
                        optionText={option}
                        count={index+1}
                        handleDeleteOption={this.props.handleDeleteOption}
                    />
                )
            }
        </div>
    )};
}
const mapStateToProps = (state) => {
    return {
        filters: state.filters
    }
}
export default connect(mapStateToProps)(Options);