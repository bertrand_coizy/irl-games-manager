import { createStore, combineReducers } from 'redux';

export const incrementGameCreationStep = () => ({
    type : 'UPDATE_PLAYER_TURN'
});

export const playStrike = (playerId, strike) => ({
    type : 'PLAY_STRIKE',
    playerId,
    strike
});

