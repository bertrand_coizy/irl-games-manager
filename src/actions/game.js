import { createStore, combineReducers } from 'redux';

export const incrementGameCreationStep = () => ({
    type : 'INCREMENT_GAME_CREATION_STEP'
});
export const decrementGameCreationStep = () => ({
    type : 'DECREMENT_GAME_CREATION_STEP'
});
export const updateGameType = (gameType) => ({
    type : 'UPDATE_GAME_TYPE',
    gameType
}); 
export const addPlayer = (player) => ({
    type : 'ADD_PLAYER',
    player
});
export const removePlayer = (player) => ({
    type : 'REMOVE_PLAYER',
    player
});
export const removePlayers = () => ({
    type : 'REMOVE_PLAYERS'
});
export const updatePlayerOrder = (oldIndex, newIndex) => ({
    type : 'UPDATE_PLAYER_ORDER',
    oldIndex,
    newIndex
})