import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, Switch} from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import Header from "../components/Header";
import DashboardPage from "../components/DashboardPage";
import CreateGamePage from "../components/GameCreation/CreateGamePage";
import StatisticsPage from "../components/StatisticsPage";
import PlayPage from "../components/Play/PlayPage";
import LoginPage from '../components/LoginPage'

export const history = createHistory();

const AppRouter = () => (
  <Router history={history}>
    <div>
      <Header/>
      <Switch>
        <Route path="/" component={LoginPage} exact={true} />
        <Route path="/dashboard" component={DashboardPage} />
        <Route path="/create" component={CreateGamePage} />
        <Route path="/statistics" component={StatisticsPage} />
        <Route path="/Play" component={PlayPage} />
      </Switch>
    </div>
  </Router>
  );

  export default AppRouter;